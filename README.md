# MyBigNumber
Đầu tiên tạo lớp MyBigNumber
Tiếp theo tạo hàm sum với 2 chuỗi stn1 và stn2
Tiếp theo trong hàm tạo biến result, carry, i, j
Tiếp theo tạo 1 vòng lặp while để duyệt qua các ký tự của 2 chuỗi cho đến khi cả 2 chuỗi đều được duyệt hết
Trong vòng lặp While tạo biến digit1, digit2, total, result, carry
Tạo lệnh if total, else trong vòng lặp while
Tạo biến i-=1 và j-=1 thể hiện rằng các ký tự tiếp theo sẽ được duyệt liên tục về phía trước
Tạo lệnh if carry trong vòng lặp while để kiểm tra xem có giá trị nhớ cuối cùng hay không
Return lại kết quả trong vòng lặp while
Cuối cùng tạo biến my_calculator bên ngoài vòng lặp while và gán lớp MyBigNumber vào rồi print kết quả
